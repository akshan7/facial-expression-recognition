import os
import dlib
import cv2
import numpy as np
import math

label_map={0:'neutral', 1:'anger', 2:'contempt', 3:'disgust', 4:'fear', 5:'happy', 6:'sadness', 7:'surprise'}

faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
faceDet_two = cv2.CascadeClassifier("haarcascade_frontalface_alt2.xml")
faceDet_three = cv2.CascadeClassifier("haarcascade_frontalface_alt.xml")
faceDet_four = cv2.CascadeClassifier("haarcascade_frontalface_alt_tree.xml")


def detect_faces(path):
    frame = cv2.imread(path) #Open image
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) #Convert image to grayscale
    # cv2.imshow('ss', gray)
    # cv2.waitKey(0)
    
    #Detect face using 4 different classifiers
    # face = faceDet.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
    faces = faceCascade.detectMultiScale(gray,scaleFactor=1.3,minNeighbors=5,minSize=(200, 200), flags = cv2.CASCADE_SCALE_IMAGE)
    # face_two = faceDet_two.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
    # face_three = faceDet_three.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
    # face_four = faceDet_four.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)

    # #Go over detected faces, stop at first detected face, return empty if no face.
    # if len(face) == 1:
    #     facefeatures = face
    # elif len(face_two) == 1:
    #     facefeatures = face_two
    # elif len(face_three) == 1:
    #     facefeatures = face_three
    # elif len(face_four) == 1:
    #     facefeatures = face_four
    # else:
    #     facefeatures = ""

    print('ssss')
    
    #Cut and save face
    for (x, y, w, h) in faces: #get coordinates and size of rectangle containing face
        # print ("face found in file: %s" %f)
        gray = gray[y:y+h, x:x+w] #Cut the frame to size
        
        try:
            out = cv2.resize(gray, (350, 350)) #Resize face so all images have same size
            # cv2.imwrite("dataset\\%s\\%s.jpg" %(emotion, filenumber), out) #Write image
        except:
           pass #If error, pass file

    return out



def extract_landmark(root_path,root_label_path):

	# print(root_path)
	# print(root_label_path)


	data=[]
	# labels=[]

	image_paths=os.listdir(root_path)
	# print(image_paths)
	images=[image_paths[(0+len(image_paths))//2+1],image_paths[-1],image_paths[(0+3*len(image_paths))//4]]
	# print(images)

	f = open(root_label_path+'\\'+os.listdir(root_label_path)[0], 'r')
	label = f.read().strip()
	# print('label',label_map[int(label[0])])
	label = [label_map[int(label[0])]]*3


	detector = dlib.get_frontal_face_detector()
	predictor = dlib.shape_predictor(r'D:\expression recognition\shape_predictor_68_face_landmarks.dat')

	for path in images:
		# print('finalpath',root_path+'\\'+path)
		img=detect_faces(root_path+'\\'+path)
		# img=cv2.imread(root_path+'\\'+path)
		cv2.imshow('img', img)
		cv2.waitKey(0)
		ss

		# dets = detector(img, 1)
		# det = detector(img, 1)[0]
		landmarks = predictor(img, detector(img, 1)[0])

		landmark_x = []
		landmark_y = []
		for i in range(68):
			landmark_x.append(landmarks.part(i).x)
			landmark_y.append(landmarks.part(i).y)

		xmean = np.mean(landmark_x)
		ymean = np.mean(landmark_y)
		xcentral = [(x-xmean) for x in landmark_x]
		ycentral = [(y-ymean) for y in landmark_y]
		landmarks_vectorised = []
		for x, y, w, z in zip(xcentral, ycentral, landmark_x, landmark_y):
			landmarks_vectorised.append(w)
			landmarks_vectorised.append(z)
			meannp = np.asarray((ymean,xmean))
			coornp = np.asarray((z,w))
			dist = np.linalg.norm(coornp-meannp)
			landmarks_vectorised.append(dist)
			landmarks_vectorised.append((math.atan2(y, x)*360)/(2*math.pi))

		data.append(landmarks_vectorised)
	return data, label






subjects=os.listdir(r'D:\expression recognition\cohn-kanade-images')
emotion=os.listdir(r'D:\expression recognition\Emotion')

# print(subjects)
# print(emotion)

data=[]
labels=[]


for i in range(len(subjects)):
	print(i)
	for j in (os.listdir(r'D:\expression recognition\cohn-kanade-images\\'+subjects[i])):
		if(j!='.DS_Store'):
			# print('cohn-kanade-images\\'+subjects[i]+'\\'+j)
			# try:
			data1, labels1=extract_landmark(r'D:\expression recognition\cohn-kanade-images\\'+subjects[i]+'\\'+j, r'D:\expression recognition\Emotion\\'+subjects[i]+'\\'+j)
			# print('datalength',len(data1), len(data1[0]))
			# print('labels',labels1)
			
			for i in range(3):
				data.append(data1[i])
				labels.append(labels1[i])

				# print(len(data))
				# print(len(labels))
				
			# except:
			# 	pass


data=np.array(data)



print(data.shape)
# print(data)
print(len(labels))
# print(labels)

np.save('features', data)
d=np.load('features.npy')

f=open('labels.txt','w')
for i in labels:
	f.write(i)
	f.write('\n')

f.close()


print('data from numpy file')
print(len(d))
print(d)



