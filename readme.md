# Facial Expression Recognition

datareader.py generates the features.npy and labels.txt files needed to train the model. These files are already uploaded.

(Download the 'shape_predictor_68_face_landmarks.dat' file from the link displayed here to run datareader.py:
https://drive.google.com/open?id=1_O2lej2Jw7GtUxhHPc19epPJhvJku5XR)

## To verify accuracy of the model: 
Run modelfit.py. It displays the cross validatioin scores in a list and also returns the final mean accuracy 
Note: Make sure features.npy and labels.txt are in the same folder as modelfit.py when you run it. 
Note: After running modelfit.py wait for 2-3 minutes to get result as it takes time to fit the model. 