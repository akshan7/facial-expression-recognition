import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import scale
from sklearn.metrics import accuracy_score
import sklearn
from sklearn.svm import SVC
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import cross_val_score 

data=np.load('features.npy')

f=open('labels.txt','r')
labels=f.read().split()

# for i in data1:
# 	data.append([i[429],i[1099],i[2559],i[3083],i[3862],i[4080],i[4277]])
# 	# print('\n')
# data=np.array(data)
print(data.shape)
print(len(labels))


# X, Xt, y, yt = train_test_split(data, labels, test_size = 0.3, random_state=10000)
classifier=GradientBoostingClassifier(random_state=1)
# classifier=GradientBoostingClassifier(learning_rate=0.11, max_depth=1, n_estimators=100)
# classifier.fit(X,y)

scores = cross_val_score(classifier, data, labels, cv=10)
print(scores)
print(np.mean(scores))

# print(y)


# classifier=SVC(kernel='linear', C=0.005)
# classifier.fit(X,y)

# y_pred=classifier.predict(Xt)
# print(accuracy_score(yt,y_pred))

# y_pred=classifier.predict(X)
# print(accuracy_score(y,y_pred))
